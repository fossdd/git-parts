# `git parts`

More friendlier handler for `git submodule`

### Installation

Currently there is no nice installation like over PKGBUILDs.

But you can simply download the file to your `/bin` directory.

```bash
curl https://codeberg.org/fossdd/git-parts/raw/branch/main/git-parts -o /bin/git-parts
chmod +x /bin/git-parts
```

### Using

By running `git parts` you have cloned and updated all git submodules.

### Mutivation

The `git submodule` command is big and has advanced features. I want to have a simple command that does direcly all like dowwnloading and keeping all up-to-date.